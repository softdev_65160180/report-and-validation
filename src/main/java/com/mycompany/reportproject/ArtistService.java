/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import com.sun.source.doctree.ReturnTree;
import java.util.List;

/**
 *
 * @author User
 */
public class ArtistService {
    public List<ArtistReport>getArtistByTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(10);
    }
    public List<ArtistReport>getArtistByTotalPrice(String begin, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(begin, end,10);
    }
    
}
